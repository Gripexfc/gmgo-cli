'use strict';
const path = require('path');
const pkgDir = require('pkg-dir').sync;
const pathExists = require('path-exists').sync;
const fsExtra = require('fs-extra');

const { isObject } = require('@gmgo-cli/utils');
const { formatePath } = require('@gmgo-cli/determine-device-type');
const npmInstall = require('npminstall');
const { getRegistry, getLatestVersion } = require('@gmgo-cli/get-npm-info');

// import pkgDir from 'pkg-dir'

module.exports = class packages {
    constructor(option) {
        if (!isObject(option)) {
            throw new Error('需要传入option为参数');
        }
        this.targetPath = option.targetPath;
        this.storeDir = option.storeDir;
        this.packageName = option.packageName;
        this.packageVersion = option.packageVersion;
        this.cachePackageName = this.packageName.replace('/', '-');
    }

    async prepare() {
        if (this.storeDir && !pathExists(this.storeDir)) {
            //创建缓存文件
            fsExtra.mkdirp(this.storeDir);
        }
        if (this.packageVersion === 'latest') {
            this.packageVersion = await getLatestVersion('foo');
        }
    }

    get cacheFilePath() {
        return path.resolve(this.storeDir, `_${this.cachePackageName}@${this.packageVersion}@${this.packageName}`)
    }

    getLatestVersionCacheFilePath(version) {
        return path.resolve(this.storeDir, `_${this.cachePackageName}@${this.version}@${this.packageName}`)
    }


    async update() {
        await this.prepare();
        this.latestPackageVersion = await getLatestVersion('foo');
        if (!pathExists(this.getLatestVersionCacheFilePath(this.latestPackageVersion))) {
            npmInstall({
                root: this.targetPath,
                pkgs: [
                  { name: 'path-exists', version: this.latestPackageVersion },
                ],
                registry: getRegistry(),
                storeDir: this.storeDir
            })
        } 
    }

    async exists() {
        if (this.storeDir) {
            await this.prepare();
            return pathExists(this.cacheFilePath);
        } else {
            return pathExists(this.targetPath)
        }
    }

    install() {
        return npmInstall({
            root: this.targetPath,
            pkgs: [
              { name: 'path-exists', version: '4.0.0' },
            ],
            registry: getRegistry(),
            storeDir: this.storeDir
        })
    }

    getRootFilePath() {
        function _getRootFile(targetPath) {
            const dir = pkgDir(targetPath);
            if (dir) {
                const pkgFile = require(path.resolve(dir, 'package.json'));

                if (pkgFile && pkgFile.main) {
                    return formatePath(path.resolve(dir, pkgFile.main));
                }
            }
            return null;
        }
        if (this.storeDir) {
            return _getRootFile(this.cacheFilePath);
        } else {
            return _getRootFile(this.targetPath);
        }
    }
}
