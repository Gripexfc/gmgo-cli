'use strict';
const semver = require('semver');
const colors = require('colors');
const log = require('@gmgo-cli/log');
const { argv } = require('yargs');

const SUPPORTNODEVERSION = 'v15.16.0'

class Command {
    constructor(argv) {
        if (!argv) {
            throw new Error('参数不能为空！！');
        }
        if (!Array.isArray(argv)) {
            throw new Error('传入参数必须为数组类型');
        }
        this._argv = argv;
        const runner = new Promise((resolve, reject) => {
            let chain = Promise.resolve();
            chain = chain.then(() => this.checkNodeVersion());
            chain = chain.then(() => this.initArgv());
            chain = chain.then(() => this.init());
            chain = chain.then(() => this.exec());
            //promise单独捕捉错误上报
            chain.catch(err =>{
                log.error(err.message);
            })
        })
        // console.log(argv,'constructor');
    }

    checkNodeVersion() {
        console.log(process.version);
        const nodeVersion = semver.gte(process.version, SUPPORTNODEVERSION);
        if (!nodeVersion) {
            log.error(colors.red('Node版本小于' + SUPPORTNODEVERSION + '请升级到更高版本'))
        }
    }

    initArgv() {
        this._cmd = this._argv[this._argv.length - 1];
        //命令option赋值给cmdObj上，方便取值
        Object.keys(this._argv[1]).map(key => {
            this._cmd[key] = this._argv[1][key];
        })
        
        this._argv = this._argv.slice(0, this._argv.length - 1);
    }

    init() {
        throw new Error('必须实现init方法！！！');
    }

    exec() {
        throw new Error('必须实现exec方法！！！');
    }

}

module.exports = Command;
