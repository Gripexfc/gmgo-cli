const path = require('path');

module.exports = {
    entry: './core/cli/bin',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'core.js'
    },
    target: 'node',
    mode: 'development',
    module: {
        rules: [
            {
              test: /\.m?js$/,
              exclude: /node_modules/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: [
                    ['@babel/preset-env', { targets: "defaults" }]
                  ]
                }
              }
            }
          ]
    }
}