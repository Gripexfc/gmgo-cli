const axios = require('axios');

const baseUrlL = process.env.GMGO_SERVE_PATH ? process.env.GMGO_SERVE_PATH : 'http://gmgo-cli:7001';

const instance = axios.create({
    baseURL: 'http://gmgo-cli:7001',
    timeout: 5000,
  });

instance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response.status === 200) {
        return response.data
    }
    
    return new Error('请求失败')
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});

async function getTemplateInfo() {
    return await instance(
    {
        method: 'GET',
        url: '/project/template'
    }
    )
}

module.exports = getTemplateInfo;



// axios({
//     method: 'get',
//     url: 'http://gmgo-cli:7001/project/template',
//   })
//     .then(function (response) {
//       console.log(response.data,'*****************************************');
//     });