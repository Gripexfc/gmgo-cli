#!/usr/bin/env node
// 'use strict';
const fs = require('fs');
const inquirer = require('inquirer');
const Command = require('@gmgo-cli/Command');
const fsExtra = require('fs-extra');
const console = require('console');
const semver = require('semver');

const getTemplateInfo = require('./getTemplateInfo');

let projectType;

class initCommand extends Command {
    init() {
      this.projectName = this._argv[0] || '';
      this.force = !!this._cmd.force;
      this.tempalteInfo = '';
      this.projectInfo = '';
      // console.log(this._cmd.tp,'实现init方法');
    }

    async exec() {
      this.projectInfo = await this.prePare();
      this.tempalteInfo = await getTemplateInfo();
      //根据数据库存储信息获取对应npm包
    }

    async prePare() {
      const localPath = process.cwd();
      //文件目录不为空
      if (!this.isEmpty(localPath)) {
        let isCover = false;
        //是否强制覆盖
        /**
         * inquirer一个交互命令集合 详情: https://www.npmjs.com/package/inquirer
         */
        if (!this.force) {
          isCover = (await inquirer
          .prompt({
            type: 'confirm',
            name: 'isCover',
            message: '是否覆盖当前文件',
            default: false,
          })).isCover
          if (!isCover) {
            return;
          }
        }
        //用户选择 || --force参数直接判断清空文件
        if (isCover || this.force) {
          //清空文件目录
          const { isEmptyFilt} = await inquirer
          .prompt({
            type: 'confirm',
            name: 'isEmptyFilt',
            message: '是否清空当前文件夹',
            default: false,
          })
          if (isEmptyFilt) {
            fsExtra.emptyDirSync(localPath);
          }
        }
      }
      
      //选择模板类型 项目、组件模板
      //获取项目信息
      return this.getProjectInfo();
    }

    async getProjectInfo() {
      //获取创建模板类型、组件
      //设置项目、组件版本号
      projectType = (await inquirer.prompt([{
        type: 'list',
        name: 'projectType',
        message: '请选择项目类型',
        choices: [
          {
            name: '项目',
            value: 'TYPE_PROJECT'
          },
          {
            name: '组件',
            value: 'TYPE_COMPONENTS'
          }
        ]
      }])).projectType;
      if (projectType === 'TYPE_PROJECT') {
        //创建项目
        const projectInfo = (await inquirer.prompt([
          {
            type: 'input',
            name: 'projectName',
            message: '请输入项目名称',
            validate: function(name) {
              // Declare function as asynchronous, and save the done callback
              const r = (/^[a-zA-Z]+([-][a-zA-Z][a-zA-Z0-9]*|[_][a-zA-Z][a-zA-Z0-9]*|[a-zA-Z0-9])*$/).test(name);
              const done = this.async();

              // Do async stuff
              setTimeout(function() {
                if (!r) {
                  // Pass the return value in the done callback
                  done('项目格式应为a-b格式！！');
                  return;
                }
                // Pass the return value in the done callback
                done(null, true);
              }, 0);
              console.log(r);
              return true;
            }
          },
          {
            type: 'input',
            name: 'projectVersion',
            message: '请输入项目版本号',
            default: '1.0.0',
            validate: function(V) {
              return !!semver.valid(V)
            },
            filter: function(V) {
              return semver.valid(V)
            }
          }
        ]))
        
        return {
          type: 'TYPE_PROJECT',
          ...projectInfo
        }
      }
    }

    isEmpty(localPath) {
      //判断文件目录是否为空
      let fileList = fs.readdirSync(localPath);
      fileList = fileList.filter((filt) => (!filt.startsWith('.') && !(filt === 'node_modules')));
      
      return !fileList || fileList.length <= 0;
    }
}  

module.exports = init;

function init(argv) {
  return new initCommand(argv);
}
// const cp = require('child_process');
// cp.exec('dir',{}, function(error, stdout, stderr) {
//     console.log(error);
//     console.log(stdout);
//     console.log(stderr);
// })


// cp.exec( 'node ls' , function(err, stdout , stderr ) {
// console.log( err ); 
// });
// // console.log(ps);

