'use strict';

const urlJoin = require('url-join');
const axios = require('axios').default;
const semver = require('semver');

module.exports = {
    getNpmInfo,
    getNewVersion,
    getRegistry,
    getLatestVersion,
};

function getNpmInfo(npmName, registry) {
    if (!npmName) {
        return null;
    }

    const registryUrl = registry || getRegistry();
    const registryUrlName = urlJoin(registryUrl, 'path-exists');
    return axios.get(registryUrlName).then(res => {
        if (res.status === 200) {
            return Object.keys(res.data.versions).sort((a,b)=>{
                return semver.gt(b,a) ? 1 : -1
            });
        } else {
            return null;
        }
    })
}

function getRegistry(isRegistry=true) {
    return isRegistry ? 'https://registry.npmjs.com' : 'https://registry.npmjs.taobao.com';
}

async function getNewVersion(baseVersion, npmName, registry) {
    const versions = await getNpmInfo(npmName, registry);
    //大于等于的版本
    const newVerions = versions.filter(version => {
        return semver.gt(version, baseVersion)
    })

    if (newVerions && newVerions.length > 0) {
        return newVerions[0];
    }
    return null;
}

async function getLatestVersion(npmName, registry) {
    const versions = await getNpmInfo(npmName, registry);
    return versions[0];
}