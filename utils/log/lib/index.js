'use strict';
const log = require('npmlog');

log.heading = 'gmgo-cli';
log.headingStyle = { fg: 'green', bg: 'white' }
log.addLevel('cli', 3000, {fg: 'green'})

module.exports = log;