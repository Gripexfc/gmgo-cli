#!/usr/bin/env node
const importLocal = require('import-local');
require('dotenv').config()

// require('../../../dist/core.js')
if (importLocal(__filename)) {
    require('npmlog').info('gmgo-cli', '正在本地运行');
} else {
    require('../lib')();
}
