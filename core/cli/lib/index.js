'use strict';
const semver = require('semver');
const colors = require('colors');
const minimist = require('minimist');
const userHome = require('user-home');
const dotenv = require('dotenv');
const fs = require('fs')
const Commander = require('commander');

const pathExists = require('path-exists').sync;
// const rootCheck = require('root-check');

const pck = require('../package.json');
const log = require('@gmgo-cli/log');
const init = require('@gmgo-cli/init');
const test = require('./test');
const path = require('path');
const { option } = require('yargs');
const exec = require('@gmgo-cli/execute');


const program = new Commander.Command();
let argv;

module.exports = core;

async function core(argv) {
    try {
        await prepare();
        loginCommand();
    } catch(err) {
        if (process.env.LOG_LEVEL === 'verbose') {
            log.verbose(err)
        } else {
            console.log(colors.red(err.message));
        }
    }
 
}

async function prepare() {
    checkPrkVersion();
    checkRoot();
    checkUserHome();
    // checkInputArgv();
    checkEnvVariable(); 
    await checkNpmVersion();
}

function loginCommand() {
    program
    .name(Object.keys(pck.bin)[0])
    .usage('gmgo-cli [option]')
    .version(pck.version)
    .option('-tp --targetPath <targetPath>', 'file path')
    .option('-d, --debug', 'output extra debugging', false);

    program
    .command('init [filePath]')
    .option('-f, --force', 'output extra debugging')
    .action(exec);

    program.on("option:targetPath", function() {
        process.env.CLI_TARGET_PATH = this.opts().targetPath
    })
    program.on('option:debug', function() {
        //开启debug模式
        const debug = this.opts().debug;
        if (debug) {
            process.env.LOG_LEVEL = 'verbose';
        } else {
            process.env.LOG_LEVEL = 'info';
        }
        log.level = process.env.LOG_LEVEL;
    })

    program.parse(process.argv)
}

async function checkNpmVersion() {
    const { getNewVersion } = require('@gmgo-cli/get-npm-info');
     
    const newVersion = await getNewVersion('3.0.0',pck.name);
    if (newVersion) {
        log.warn(colors.yellow(`请手动更新${pck.name},当前版本${pck.version},最新版本为${newVersion}`))
    }
}

function checkEnvVariable() {
    dotenv.config({
        path: path.resolve(userHome, '.env')
    })
    log.info(process.env.HOMECLI)
    // console.log(process.env.HOMECLI)
}

function checkInputArgv() {
    // console.log(dotenv.config().HOMECLI)
    // argv = minimist(process.argv.splice(2))
    // console.log(argv,'argv');
    // if (argv.debug) {
    //     //debuger模式下输出
    //     log.level = 'verbose';
    //     console.log('log')
    // }
    // log.verbose('debug模式')
}

function checkUserHome() {
    const isPathExists = pathExists(userHome);
    if (!isPathExists) {
         new Error('用户主目录不存在！！！')
    }
}

function checkRoot() {
    
    // rootCheck();
    // const uid = process.geteuid();
    // console.log(uid)
    // console.log(process.getuid(),'process.getuid()')
    // console.log(process.geteuid(),'uid')
}

function checkPrkVersion() {
    log.cli(pck.version)
}
