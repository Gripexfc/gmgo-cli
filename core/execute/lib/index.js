'use strict';
const path = require('path');
const cp = require('child_process');
const PackageModules = require('@gmgo-cli/Packages');
const log = require('@gmgo-cli/log');


const SET_PACKAGE_NAME = {
    init: 'path-exists'
}
const CACHE_PATH =  'dependencies';

module.exports = execute;

function execute() {
    let targetPath = process.env.CLI_TARGET_PATH;
    const homePath = process.env.HOME_PATH;
    log.verbose(targetPath);
    log.verbose(homePath);
    const cmdObj = arguments[arguments.length -1];
    // console.log(cmdObj.options,'cmdObj');
    const cmdName = cmdObj.name();
    const packageName = SET_PACKAGE_NAME[cmdName];
    const packageVersion = '5.0.0';
    let storeDir, packageModules;

    if (!targetPath) {
        //缓存路径
        targetPath = path.resolve(homePath, CACHE_PATH);
        storeDir = path.resolve(targetPath, 'node_modules');

        packageModules = new PackageModules({
            targetPath,
            storeDir,
            packageName,
            packageVersion,
        })
        packageModules.update();
        if (packageModules.exists()) {
            //更新packages
            packageModules.update();
        } else {
            console.log('install');
            //安装packages
            packageModules.install();
        }
    } else {
        packageModules = new PackageModules({
            targetPath,
            storeDir,
            packageName,
            packageVersion
        })
    }
    const rootPath = packageModules.getRootFilePath();
    if (rootPath) {
        try {
            const argv = Array.from(arguments);
            let o = Object.create(null);
            let cmdObj = argv[argv.length - 1];
            //去除无用arguments中的属性
            Object.keys(cmdObj).map(key => {
                if (cmdObj.hasOwnProperty(key) && !key.startsWith('_') && key !== 'parent') {
                    o[key] = cmdObj[key];
                }
            })
            argv[argv.length - 1] = o;
            //node多进程优化init命令  详情：https://nodejs.org/dist/latest-v18.x/docs/api/child_process.html#child_processexeccommand-options-callback
            const code = `require('${rootPath}').call(null, ${JSON.stringify(argv)})`;
            const child = spawn('node', ['-e', code], {
                cwd: process.cwd(),
                stdio: 'inherit'
            });
            child.on('error',e=> {
                console.log(e.message); 
                process.exit(e)
            })
            child.on('exit',e=> {
                // console.log('命令执行成功' + e);
                // process.exit(e)
            })
        } catch (error) {
            log.error(error.message);
        }
    }
}
//兼容windows系统
function spawn(command, argv, options) {
    const cmd = process.platform === 'win32' ? 'cmd' : command;
    const cmdArgv = process.platform === 'win32' ? ['/c'].concat(command, argv) : argv ;

    return cp.spawn(cmd, cmdArgv, options || {});
}
